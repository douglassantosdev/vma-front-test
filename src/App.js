import React from 'react';
import './App.css';
import UserPage from './components/pages/user';
import Home from './components/pages/home';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/user" render={(props) => <UserPage {...props}/>}/>
        <Route path="/" render={() => <Home/>}/>
      </Switch>
    </Router>
  );
}

export default App;
