import axios from 'axios';

const apiUrl = 'http://localhost:5000';
const options =  { headers: { 'Authorization': 'Example of a token that would be retrieved on login'}}

export const listUsers =  async (params) => {
    const queryParams = params.filter(({value})=> value !== '' )
                              .reduce((acc, {key, value}) => `${acc}${acc === '?' ? '' : '&'}${key}=${value}`, '?');
    const response = await axios(`${apiUrl}/v1/users${queryParams}`, options);

    return response;
}

export const getUser = async (id) => {
    const response = await axios(`${apiUrl}/v1/user/${id}`, options);

    return response.data;   
}

export const updateUser = (caseToUpdate) => {
    return axios.put(`${apiUrl}/v1/user`, caseToUpdate, options);
}

export const createUser = (caseToCreate) => {
    return axios.post(`${apiUrl}/v1/user`, caseToCreate, options);
}

export const deleteUser = (id) => {
    return axios.delete(`${apiUrl}/v1/user/${id}`, options);
}
