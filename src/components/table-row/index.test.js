import React from 'react';
import { render} from '@testing-library/react';
import {BrowserRouter as Router} from "react-router-dom";
import TableRow from './index';

describe('Testing table-row component', ()=>{

  it('renders rows', () => {
    const columns = [
        {label: 'Name', key: 'name'},
        {label: 'Email', key: 'email'}
      ];
    const rows = [
        { "id": 1, "name": "douglas", "email": "d1@gmail.com"},
        { "id": 1, "name": "teste", "email": "t1@gmail.com"},
    ];
    const { getByText } = render(
        <Router>
            <table>
                <tbody>
                    <TableRow columns={columns} rows={rows} onRowClick={()=>{}}/>
                </tbody>
            </table>
        </Router>);

    expect(getByText(/douglas/i)).toBeInTheDocument();
    expect(getByText(/d1@gmail.com/i)).toBeInTheDocument();
    expect(getByText(/teste/i)).toBeInTheDocument();
    expect(getByText(/t1@gmail.com/i)).toBeInTheDocument();
  });
});
