import React from 'react';
import { Link } from "react-router-dom";
import uuid from 'react-uuid';

const TableRow = ({columns, rows, onRowClick, setTableReloadTrigger}) => {
    const buttons = columns.filter(rows => rows.type === 'button');
    const cols = columns.filter(rows => rows.type !== 'button');

    const onRowButtonClicked = async ({confirmMessage, action, successMessage, errorMessage, id, refreshTable}) => {
        try {
            const confirmation = confirmMessage && window.confirm(confirmMessage);
            if (confirmMessage  && confirmation !== true) {
                return;
            }
            await action(id);

            if(successMessage){
                alert(successMessage)
            }
            if(refreshTable){
                setTableReloadTrigger(true);
            }
        } catch (error) {
            //here we can improove and log it properly
            alert(errorMessage)
        }
    }

    return rows.map((row) => 
        <tr key={uuid()}>
            {cols.map(({key}) => 
                <td key={uuid()}><Link to={{pathname: onRowClick, id: row['id'] }}>{row[key]}</Link></td> 
            )} 
            {buttons.map((button) => 
                <td key={uuid()}>
                    <button onClick={(e) => { onRowButtonClicked({...button, id: row['id']})}}>{button.label}</button>
                </td> 
            )}
        </tr>
    );
}

export default TableRow;