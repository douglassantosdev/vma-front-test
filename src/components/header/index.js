import React from 'react';
import styled from 'styled-components';
import logo from '../../assets/logo.png'

const HeaderWraper = styled.section` background: #f9ac19; `;
const headerLogo = logo;

const Header = () => {
    return  <HeaderWraper><img src={headerLogo} alt="Logo"/></HeaderWraper>       
}

export default Header;
