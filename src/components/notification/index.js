import React from 'react';
import styled from 'styled-components';

const SuccessNotification = styled.section` 
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    width: 50%;
    margin-left: 30%;
    height: 40px;
    padding-top: 1em;
    margin-top: 1em;
`;

const ErrorNotification = styled.section`
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
    width: 50%;
    margin-left: 30%;
    height: 60px;
    padding-top: 1em;
    margin-top: 1em;
`


const Notification = ({errors, message}) => {
    const nonNullErrors = errors ? errors.filter(error => error !== null) : null;
    if(nonNullErrors && nonNullErrors.length > 0) {
        return <ErrorNotification>{errors.map(e => [e, <br/>] )}</ErrorNotification>          
    }        
    if(!message || message === '') {
        return null;
    }

    return <SuccessNotification>{message}</SuccessNotification>  
}

export default Notification;