import React, { useState } from 'react';

const TablePaginator = ({pagination, setCurrentPageToFetchDataFilters, setTableReloadTrigger}) => {
    const pieces = Object.keys(pagination).length > 0 ? pagination.replace('bytes ', '').split('-') : null;
    const from = pieces ? parseInt(pieces[0]) : 0;
    const pageSize = 10;
    const totalRows = Object.keys(pagination).length > 0 ? parseInt(pagination.split('/')[1]) : 0;
    const totalPages = Math.ceil(totalRows/pageSize) ;
    const [currentPage, setCurrentPage] = useState((from > 0 ? from/pageSize : 1));

    const onPaginationClick = (pageToGo) => {
        const page = pageToGo < 1 ? 1 : (pageToGo > totalPages ? totalPages: pageToGo);
        setTableReloadTrigger(true);
        setCurrentPageToFetchDataFilters(page);
        setCurrentPage(page);
    }

    return <div> 
                <a href="#" onClick={() => onPaginationClick(currentPage-1)}> Prev </a> 
                    {currentPage}
                <a href="#" onClick={() => onPaginationClick(currentPage+1)}> Next </a> 
                of {totalPages} pages and {totalRows} rows
            </div>
}

export default TablePaginator;
