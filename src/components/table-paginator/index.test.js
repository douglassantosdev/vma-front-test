import React from 'react';
import { render, fireEvent, screen} from '@testing-library/react';
import TablePaginator from './index';

describe('Testing table-paginator component', ()=>{

  it('renders current and total pages', () => {
    const pagination = 'bytes 1-20/200';
    const { getByText } = render(<TablePaginator pagination={pagination} />);

    expect(getByText(/of 20 pages and 200 rows/i)).toBeInTheDocument();
  });

  it('test prev click', () => {
    const setTableReloadTrigger = (isToReload) => {
      expect(isToReload).toBe(true);
    }
    
    const setCurrentPageToFetchDataFilters = (currentPage) => {
      expect(currentPage).toEqual(9);
    }

    const pagination = 'bytes 100-110/200';

    render(<TablePaginator 
            pagination={pagination}
            setTableReloadTrigger={setTableReloadTrigger} 
            setCurrentPageToFetchDataFilters={setCurrentPageToFetchDataFilters}
    />);
    fireEvent.click(screen.getByText(/Prev/i))  
  });

  it('test prev click and ensure it does not go under 1', () => {

    const setTableReloadTrigger = (isToReload) => {
      expect(isToReload).toBe(true);
    }
    
    const setCurrentPageToFetchDataFilters = (currentPage) => {
      expect(currentPage).toEqual(1);
    }

    const pagination = 'bytes 0-10/200';

    render(<TablePaginator 
            pagination={pagination}
            setTableReloadTrigger={setTableReloadTrigger} 
            setCurrentPageToFetchDataFilters={setCurrentPageToFetchDataFilters}
    />);
    fireEvent.click(screen.getByText(/Prev/i))    
  });

  it('test next click', () => {

    const setTableReloadTrigger = (isToReload) => {
      expect(isToReload).toBe(true);
    }
    
    const setCurrentPageToFetchDataFilters = (currentPage) => {
      expect(currentPage).toEqual(11);
    }

    const pagination = 'bytes 100-110/200';
  
    render(<TablePaginator 
            pagination={pagination}
            setTableReloadTrigger={setTableReloadTrigger} 
            setCurrentPageToFetchDataFilters={setCurrentPageToFetchDataFilters}
    />);
    fireEvent.click(screen.getByText(/Next/i))    
  });

  it('test next click does not go over total pages', () => {

    const setTableReloadTrigger = (isToReload) => {
      expect(isToReload).toBe(true);
    }
    
    const setCurrentPageToFetchDataFilters = (currentPage) => {
      expect(currentPage).toEqual(20);
    }

    const pagination = 'bytes 190-200/200';

    render(<TablePaginator 
            pagination={pagination}
            setTableReloadTrigger={setTableReloadTrigger} 
            setCurrentPageToFetchDataFilters={setCurrentPageToFetchDataFilters}
    />);
    fireEvent.click(screen.getByText(/Next/i))    
  });
});
