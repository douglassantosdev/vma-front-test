import React from 'react';
import { render, fireEvent} from '@testing-library/react';
import TableAddButton from './index';

describe('Testing table-add-button component', ()=>{

  it('renders add button', () => {

    const history = { 
        push: (page) =>{
            expect(page).toEqual('/goToThisPage');
        }
    }
    const addAction = {label: 'click here', route: '/goToThisPage'};
    const { getByText } = render(<TableAddButton history={history} addAction={addAction}/>);
    
    expect(getByText(/click here/i)).toBeInTheDocument();

    fireEvent.click(getByText(/click here/i))
  });
});
