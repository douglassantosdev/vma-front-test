import React from 'react';

const TableAddButton = ({addAction, history}) => {
    if (!addAction) {
        return null;
    }
    const {label, buttonName, route} = addAction;

    return <button onClick={() => history.push(route)} name={buttonName ? buttonName : 'addButton'}>{label}</button>;
}

export default TableAddButton