import React, { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";
import TablePaginator from '../table-paginator';
import TableFilter from '../table-filter';
import TableRow from '../table-row';
import TableHeader from '../table-header';
import TableAddButton from '../table-add-button';

const Table = ({columns, filters, addAction, onRowClick, data, title, titleWhenEmpty}) => {
    const [rows, setData] = useState([]);
    const [isToReloadTableData, setReloadDataTrigger] = useState(true);
    const [currentFilters, setFilters] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [pagination, setPagination] = useState({});
    const history = useHistory();

    useEffect(() => {
        if(!isToReloadTableData){
            return;
        }

        data([...currentFilters, {key: 'page', value: currentPage}]).then((data)=>{
            setData(data.data);
            setPagination(data.headers['content-range']);
         })
        setReloadDataTrigger(false)
    }, [isToReloadTableData, currentPage]);

    if(rows.length === 0){
        return <div><h2>{titleWhenEmpty}</h2><TableAddButton history={history} addAction={addAction}/></div>
    }

    return <div>
                <h2>{title}</h2>
                <TableFilter
                    filters={filters}
                    setFilters={setFilters}
                    currentFilters={currentFilters}
                    setReloadDataTrigger={setReloadDataTrigger}/>
                <TableAddButton history={history} addAction={addAction} />
                <table>
                    <TableHeader columns={columns}/>
                    <tbody>
                        <TableRow columns={columns} setTableReloadTrigger={setReloadDataTrigger} rows={rows} onRowClick={onRowClick}/>
                    </tbody>
                </table>
                <TablePaginator 
                    pagination={pagination}  
                    setCurrentPageToFetchDataFilters={setCurrentPage} 
                    setTableReloadTrigger={setReloadDataTrigger}/> 
            </div>
}

export default Table;
