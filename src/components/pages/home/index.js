import React from 'react';
import Header from '../../header';
import Table from '../../table';
import { listUsers, deleteUser} from '../../../clients/api.client';

const Home = () => {
  return (
    <div className="App">
      <Header></Header>
      <Table
        title={'Users'}
        titleWhenEmpty={'No users fount'} 
        data={listUsers} 
        columns={[
                  {label: 'Id', key: 'id'},
                  {label: 'Name', key: 'name'},
                  {label: 'Age', key: 'age'},
                  {label: 'E-mail', key: 'email'},
                  {label: 'Created At', key: 'createdAt'},
                  {
                    label: 'Delete', 
                    key: 'delete', 
                    type: 'button', 
                    confirmMessage: 'Arey you sure you want to delete this row?',
                    successMessage: 'User succesfully deleted!',
                    errorMessage: 'Some error happened while deleting the user, try again!',
                    action: deleteUser,
                    refreshTable: listUsers
                  }
                ]}  
        addAction={{label: 'New User',  route:'/user'}}
        onRowClick='/user'
        filters={[
                  {placeholder: 'Name', key: 'name'},
                  {placeholder: 'E-mail', key: 'email'}
              ]}
        />
    </div>
  );
}

export default Home;
