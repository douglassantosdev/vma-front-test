import React from 'react';
import { render } from '@testing-library/react';
import UserPage from './index';
import {BrowserRouter as Router} from "react-router-dom";

describe('Testing user page', () => {
  it('Test fields render', () => {
    const { getByText } = render(<Router><UserPage location={{}}/></Router>);

    expect(getByText(/Name/i)).toBeInTheDocument();
    expect(getByText(/Age/i)).toBeInTheDocument();
    expect(getByText(/Email/i)).toBeInTheDocument();
    expect(getByText(/Password/i)).toBeInTheDocument();
    expect(getByText(/Country/i)).toBeInTheDocument();
    expect(getByText(/State/i)).toBeInTheDocument();
    expect(getByText(/City/i)).toBeInTheDocument();
    expect(getByText(/Street/i)).toBeInTheDocument();
    expect(getByText(/Number/i)).toBeInTheDocument();
    expect(getByText(/zipCode/i)).toBeInTheDocument();
  });
});
