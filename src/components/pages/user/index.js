import React, { useState, useEffect } from 'react';
import Header from '../../header';
import Notification from '../../notification';
import { getUser, createUser, updateUser } from '../../../clients/api.client';
import { Link } from "react-router-dom";
import styled from 'styled-components';

const UPDATE_DATA_LABEL = 'Update';
const SAVE_DATA_LABEL = 'Create';
const SUCCESS_OPERATION_MESSAGE = 'Sucess!';

const ValidationSection = styled.section` 
    color: red;
`;

const UserPage = (props) => {
   const [currentUser, setCurrentUser] = useState({address:{}});
   const [errors, setErrors] = useState([]);
   const [notificationMessage, setNotificationMessage] = useState(null);
   const [nameValidation, setNameValidation] = useState('');
   const [ageValidation, setAgeValidation] = useState('');
   const [emailValidation, setEmailValidation] = useState('');
   const [passwordValidation, setPasswordValidation] = useState('');
   const [addressCountryValidation, setAddressCountryValidation] = useState('');
   const [addressStateValidation, setAddressStateValidation] = useState('');
   const [addressCityValidation, setAddressCityValidation] = useState('');
   const [addressStreetValidation, setAddressStreetValidation] = useState('');
   const [addressNumberValidation, setAddressNumberValidation] = useState('');
   const [addressZipCodeValidation, setAddressZipCodeValidation] = useState('');
   const BUTTON_LABEL = currentUser && currentUser.id ? UPDATE_DATA_LABEL : SAVE_DATA_LABEL;

   const addValidation = (setter, field, validations) => {
      setter(validations.find(v => v.field === field) ? validations.find(v => v.field === field).msg : '');  
   }

   useEffect(() => {
      if(!props.location.id){
         setCurrentUser({address:{}})

         return;
      }

      getUser(props.location.id).then(setCurrentUser);
   }, []);

   const saveOrUpdate = () => {
      const action = BUTTON_LABEL === UPDATE_DATA_LABEL ? updateUser : createUser;
      action(currentUser)
         .then( e => {
            setNotificationMessage(SUCCESS_OPERATION_MESSAGE)
         })
         .catch( e => {
            addValidation(setNameValidation, 'name', e.response.data);
            addValidation(setAgeValidation, 'age', e.response.data);
            addValidation(setEmailValidation, 'email', e.response.data);
            addValidation(setEmailValidation, 'email', e.response.data);
            addValidation(setPasswordValidation, 'password', e.response.data);
            addValidation(setAddressCountryValidation, 'address.country', e.response.data);
            addValidation(setAddressStateValidation, 'address.state', e.response.data);
            addValidation(setAddressCityValidation, 'address.city', e.response.data);
            addValidation(setAddressStreetValidation, 'address.street', e.response.data);
            addValidation(setAddressNumberValidation, 'address.number', e.response.data);
            addValidation(setAddressZipCodeValidation, 'address.zipCode', e.response.data);
         })
    };

   const handleInputChange = event => {
      setErrors([]);
      const { name, value } = event.target;
      setCurrentUser(name.indexOf('.') < 0 
         ? { ...currentUser, [name]: value }
         : { ...currentUser, [name.split('.')[0]]: {...currentUser[[name.split('.')[0]]],  [name.split('.')[1]]: value }}
      )
   }

   return <div className="App">
            <Header></Header>
            <Notification errors={errors} message={notificationMessage}></Notification>
            <form>
               <div>
                  <label>Name: </label>
                  <input name='name' value={currentUser.name} onChange={handleInputChange}/>
                  <br/>
                  <ValidationSection>{nameValidation}</ValidationSection>
               </div>
               <div>
                  <label>Age: </label>
                  <input name='age' type='number' value={currentUser.age} onChange={handleInputChange}/>
                  <ValidationSection>{ageValidation}</ValidationSection>
               </div>
               <div>
                  <label>Email: </label>
                  <input name='email' value={currentUser.email} onChange={handleInputChange}/>
                  <ValidationSection>{emailValidation}</ValidationSection>
               </div>
               <div>
                  <label>Password: </label>
                  <input name='password' type='password' value={currentUser.password} onChange={handleInputChange}/>
                  <ValidationSection>{passwordValidation}</ValidationSection>
               </div>
               <div>
                  <label>Country: </label>
                  <input name='address.country' value={currentUser.address.country} onChange={handleInputChange}/>
                  <ValidationSection>{addressCountryValidation}</ValidationSection>
               </div>
               <div>
                  <label>State: </label>
                  <input name='address.state' value={currentUser.address.state} onChange={handleInputChange}/>
                  <ValidationSection>{addressStateValidation}</ValidationSection>
               </div>
               <div>
                  <label>City: </label>
                  <input name='address.city' value={currentUser.address.city} onChange={handleInputChange}/>
                  <ValidationSection>{addressCityValidation}</ValidationSection>
               </div>
               <div>
                  <label>Street: </label>
                  <input name='address.street' value={currentUser.address.street} onChange={handleInputChange}/>
                  <ValidationSection>{addressStreetValidation}</ValidationSection>
               </div>
               <div>
                  <label>Number: </label>
                  <input 
                     name='address.number' 
                     type='number' 
                     value={currentUser.address.number} onChange={handleInputChange}/>
                  <ValidationSection>{addressNumberValidation}</ValidationSection>
               </div>
               <div>
                  <label>ZipCode: </label>
                  <input 
                     name='address.zipCode' 
                     type='number' 
                     value={currentUser.address.zipCode} onChange={handleInputChange}/>
                  <ValidationSection>{addressZipCodeValidation}</ValidationSection>
               </div>
               <div>
                  <button type='button' onClick={saveOrUpdate}>{BUTTON_LABEL}</button>
                  <Link to={{pathname: '/' }}>Voltar</Link>
               </div>
            </form>
         </div>
}

export default UserPage;
