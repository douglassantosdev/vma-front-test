import React from 'react';
import { render } from '@testing-library/react';
import TableHeader from './index';

describe('Testing table-header component', ()=>{

  it('renders table header', () => {

    const columns = [
        {label: 'Name', key: 'name'},
        {label: 'Age', key: 'age'},
        {label: 'Email', key: 'email'}
    ];
    const { getByText } = render(<table><TableHeader columns={columns}/></table>);
    
    expect(getByText(/Name/i)).toBeInTheDocument();
    expect(getByText(/Age/i)).toBeInTheDocument();
    expect(getByText(/Email/i)).toBeInTheDocument();
  });
});
