import React from 'react';
import uuid from 'react-uuid';

const TableHeader = ({columns}) => {
    return <thead><tr key={uuid()}>{columns.map(({label}) => <th key={uuid()}>{label}</th> )}</tr></thead>
}

export default TableHeader;
