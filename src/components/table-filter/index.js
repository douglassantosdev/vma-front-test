import React from 'react';

const TableFilter = ({filters, setFilters, currentFilters, setReloadDataTrigger}) => {
  const handleFilters = (key, value) => {
    if(!value) {
      setFilters([currentFilters.filter(({val}) => val !== value)]); 

      return;
    }

    setFilters([...currentFilters.filter(item => item.key !== key), {key, value}]) 
  }

  const buildInputElements = (placeholder, key) => { 
      return <input key={key} onBlur={(e) => handleFilters(key, e.target.value)} placeholder={placeholder}/>;
  }
  
  return [
    filters.filter(({type}) => !type || type === 'input').map(
      ({placeholder, key}) => buildInputElements(placeholder, key)
    ), 
    <button key='searchButtonKey' onClick={() => setReloadDataTrigger(true)} name='filter'> Search </button>
  ];
}

export default TableFilter;