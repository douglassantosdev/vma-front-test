import React from 'react';
import { render, fireEvent, screen} from '@testing-library/react';
import TableFilter from './index';

describe('Testing table-filter component', ()=>{

  it('renders filters', () => {
    const setFilters = (filters) => {
      expect(filters).toBe([]);
    }
    const setReloadDataTrigger = (isToReload) => {
      expect(isToReload).toBe(true);
    }

    baseRender(setFilters, setReloadDataTrigger);
    fireEvent.click(screen.getByText(/Search/i))
  });

  it('set input filters', () => {
    const setFilters = (filters) => {
      expect(filters).toEqual([{"key": "name", "value": "Douglas"}]);
    }
    const setReloadDataTrigger = (isToReload) => {
      expect(isToReload).toBe(true);
    }

    baseRender(setFilters, setReloadDataTrigger);

    fireEvent.blur(screen.getByPlaceholderText('Name'), {
      target: {value: 'Douglas'},
    });
    
    fireEvent.click(screen.getByText(/Search/i))
  });

  const baseRender = (setFilters, setReloadDataTrigger) => {
    const filters = [
      {placeholder: 'Name', key: 'name'},
      {placeholder: 'Email', key: 'email'}
    ];
    
    const currentFilters = [];

    render(<TableFilter 
          filters={filters}
          setFilters={setFilters}
          currentFilters={currentFilters}
          setReloadDataTrigger={setReloadDataTrigger}
    />);
  };
});
