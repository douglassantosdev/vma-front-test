[![codebeat badge](https://codebeat.co/badges/4d52c226-7f0d-449f-936e-a25a5758f737)](https://codebeat.co/projects/gitlab-com-douglassantosdev-vma-front-test-master)

### VMA Front
This is the VMA front End test, this project uses an API running on localhost:5000 so before starting it
ensure following steps are done:

- You have the API started on Port 5000 (see API README)
- You have the origin locahost:3000 configured on the API (see API README)

Link to the API project: https://gitlab.com/douglassantosdev/vma-api-test

### Setup
 - `npm install`
 
### Start
 - `npm start`

### Tests
 - `npm test`
